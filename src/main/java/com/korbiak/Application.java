package com.korbiak;

import java.util.Scanner;

/**
 * @author Maksym Korbiak
 */
public class Application {
    /**
     * task02_basic.
     * @param args args
     */
    public static void main(final String[] args) {
        /**
         * This is the main method
         */
        Scanner scanner = new Scanner(System.in);
        int start = 0;
        int end = 0;
        int n = 0;
        /**
         * Set interval for 2-3 task
         */
        System.out.print("Set interval:");
        start = scanner.nextInt();
        end = scanner.nextInt();
        Number.printNumbers(start, end);
        Number.printSum(start, end);
        /**
         * Set N for 4-5
         */
        System.out.print("\nSet n:");
        n = scanner.nextInt();
        Fibonacci.buildNumbers(n);
        Fibonacci.percentageOfNumbers(n);
    }
}
