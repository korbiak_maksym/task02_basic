package com.korbiak;

import java.util.Arrays;

/**
 * Fibonacci.
 *
 * @author Maksym Korbiak
 */
public final class Fibonacci {
    /**
     * Magic number.
     **/
    static final int HUNDRED = 100;

    /**
     * Utility classes should not have a public or default constructor.
     */
    private Fibonacci() {
    }

    /**
     * Method build Fibonacci numbers: F1 will be the .
     * biggest odd number and F2 –.
     * the biggest even number, user can enter the size of set (N).
     *
     * @param n N
     */
    public static void buildNumbers(final int n) {
        int[] numbers = new int[n];
        int f1 = 0;
        int f2 = 0;
        numbers[0] = 0;
        numbers[1] = 1;
        for (int i = 2; i < n; i++) {
            numbers[i] = numbers[i - 1] + numbers[i - 2];
            if (numbers[i] % 2 == 0) {
                f2 = numbers[i];
            } else {
                f1 = numbers[i];
            }
        }
        /**
         * Using standard output stream
         * for giving the output.
         */
        System.out.println(Arrays.toString(numbers));
        System.out.println("F1 = " + f1);
        System.out.println("F2 = " + f2);
    }

    /**
     * Method prints percentage of odd and even Fibonacci numbers.
     *
     * @param n N
     */
    public static void percentageOfNumbers(final int n) {
        int[] numbers = new int[n];
        numbers[0] = 0;
        numbers[1] = 1;
        int odd = 0;
        int even = 0;
        for (int i = 2; i < n; i++) {
            numbers[i] = numbers[i - 1] + numbers[i - 2];
        }
        for (int i = 0; i < n; i++) {
            if (numbers[i] % 2 == 0) {
                even += 1;
            } else {
                odd += 1;
            }
        }
        double prOdd = Math.round((double) odd * HUNDRED / n);
        double prEven = Math.round((double) even * HUNDRED / n);
        /**
         * Using standard output stream
         * for giving the output.
         */
        System.out.println("Odd = " + prOdd + "%");
        System.out.println("Even = " + prEven + "%");
    }
}
