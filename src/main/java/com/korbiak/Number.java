package com.korbiak;

/**
 * Number.
 *
 * @author Maksym Korbiak
 */
public final class Number {
    /**
     * Utility classes should not have a public or default constructor.
     */
    private Number() {
    }

    /**
     * Method prints odd numbers from start to the end of.
     * interval and even from end to start;
     *
     * @param start Start of interval
     * @param end   End of interval
     */
    public static void printNumbers(final int start, final int end) {
        /**
         * Using standard output stream
         * for giving the output.
         */
        System.out.print("Odd numbers:");
        for (int i = start; i <= end; i++) {
            System.out.print((i % 2 == 0 ? "" : i) + " ");
        }
        /**
         * Using standard output stream
         * for giving the output.
         */
        System.out.print("\nEven numbers:");
        for (int i = end; i >= start; i--) {
            System.out.print((i % 2 == 0 ? i : "") + " ");
        }
    }

    /**
     * Method prints the sum of odd and even numbers.
     *
     * @param start Start of interval
     * @param end   End of interval
     */
    public static void printSum(final int start, final int end) {
        int sumEven = 0;
        int sumOdd = 0;
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                sumEven += i;
            } else {
                sumOdd += i;
            }
        }
        /**
         * Using standard output stream
         * for giving the output.
         */
        System.out.print("\nSum of odd numbers:" + sumOdd);
        System.out.print("\nSum of even numbers:" + sumEven);
    }
}
